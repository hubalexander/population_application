import React from "react";
import { NavLink } from "react-router-dom";
import "./css/Home.css";

//Navigation links to the main functionalities of the application, country- and city search. 
const Home = () => {
  return (
    <div className="searchTools">
      <NavLink to="/city">
        <button>SEARCH BY CITY</button>
      </NavLink>
      <NavLink to="/country">
        <button>SEARCH BY COUNTRY</button>
      </NavLink>
    </div>
  );
};

export default Home;