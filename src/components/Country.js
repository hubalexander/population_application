import React, { useEffect, useState } from "react";
import axios from "axios";
import "./css/Country.css";

//Functional compenent. Declaration of various state hook values.
function Country() {
  const [data, setData] = useState([]);
  const [url, setUrl] = useState({});
  const [value, setValue] = useState("");
  const [cityPopulation, setCityPopulation] = useState("");
  const [cityName, setCityName] = useState("");
  const [clickBoolean, setClickBoolean] = useState(false);

  //Returns the top three populated citites.
  const topThreePopulation = (populations) => {
    let literal = {};

    for (let city in populations.geonames) {
      literal[populations.geonames[city].name] =
        populations.geonames[city].population;
    }

    return Object.keys(literal);
  };

  //Fetches the country metadata (according to the user input), and then uses the countrycode to fetch matching city data.
  useEffect(() => {
    const fetchData = async () => {
      try {
        const firstResult = await axios(url);
        const countryCode = firstResult.data.geonames[0].countryCode;
        const secondUrl = `http://api.geonames.org/searchJSON?country=${countryCode}&featureClass=P&maxRows=3&orderby=population&username=weknowit`;
        const secondResult = await axios(secondUrl);
        const secondResultData = topThreePopulation(secondResult.data);
        setData(secondResultData);
      } catch (error) {
        console.log(error);
        value ? alert("Invalid input") : console.log("empty input");
      }
    };

    fetchData();
  }, [url]);

  //Returns the population of the clicked city from the displayed country list.
  const buttonFetch = async (city) => {
    try {
      const resp = await axios(
        `http://api.geonames.org/searchJSON?name=${JSON.stringify(
          city
        )}&featureClass=P&maxRows=1&username=weknowit`
      );
      console.log(resp.data.geonames[0].population);
      setCityPopulation(resp.data.geonames[0].population);
      setCityName(city);
    } catch (error) {
      console.log(error);
    }
  };

  //Renders the site. User input initially returns either a list of three cities or error message, depending on the user input. 
  //Clicking one of the displayed ciities returns the population number of that city.
  return (
    <div>
      {!clickBoolean && (!data[2]) ? (
        <p className="countrySubmit">
          <form
            onSubmit={(e) => {
              setUrl(
                `http://api.geonames.org/searchJSON?name="${value}"&maxRows=1&username=weknowit`
              );
              e.preventDefault();
            }}
          >
            <center>SEARCH BY COUNTRY</center>
            <br />
            <input
              type="text"
              placeholder="Enter a country"
              value={value}
              onChange={(e) => setValue(e.target.value)}
            ></input>
            <input type="submit" value="Search"></input>
          </form>
        </p>
      ) : null}
      {data[0] && !clickBoolean ? (
        <p className="searchButtons">
          <br />
          <button
            onClick={() => {
              buttonFetch(data[0]) && setClickBoolean(true);
            }}
          >
            {data[0]}
          </button>

          <button
            onClick={() => {
              buttonFetch(data[1]) && setClickBoolean(true);
            }}
          >
            {data[1]}
          </button>
          <button
            onClick={() => {
              buttonFetch(data[2]) && setClickBoolean(true);
            }}
          >
            {data[2]}
          </button>
        </p>
      ) : null}
      {clickBoolean ? (
        <p className="cityResult">
          {cityName}
          <br />
          Population: {cityPopulation}
        </p>
      ) : null}
    </div>
  );
}

export default Country;