import React from "react";
import "./css/Navigation.css";

import { NavLink } from "react-router-dom";

//Navigation link to the home site, which is supposed be be reachable in every part of the application. 
const Navigation = () => {
  return (
    <div>
      <br />
      <br />
      <br />
      <br />
      <NavLink className="navigationBar" to="/">
        CityPop
      </NavLink>
    </div>
  );
};

export default Navigation;