import React, { useEffect, useState } from "react";
import axios from "axios";
import "./css/City.css";

//Functional compenent. Declaration of various state hook values. 
function City() {
  const [data, setData] = useState([]);
  const [url, setUrl] = useState({});
  const [value, setValue] = useState("");
  const [clickBoolean, setClickBoolean] = useState(false);

  //Fetches the JSON-file with axios, containing the metadata matching the user input (city name). Updates the clickboolean and data state.
  useEffect(() => {
    const fetchData = async () => {
      try {
        const result = await axios(url);
        setData(result.data.geonames[0]);
        setClickBoolean(true);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, [url]);

  //Transforms the user input to "first letter uppercase"-format. 
  const firstUpper = (cityName) => {
    if (typeof cityName !== "string") return "";
    return cityName
      .split(" ")
      .map((s) => s.charAt(0).toUpperCase() + s.slice(1).toLowerCase())
      .join(" ");
  };

  //Renders of the component. City population or error messages are displayed depending on the user input
  return (
    <div>
      {!clickBoolean ? (
        <p className="citySubmit">
        <form
          onSubmit={(e) => {
            setUrl(
              `http://api.geonames.org/searchJSON?name=${value}&featureClass=P&maxRows=1&username=weknowit`
            );
            e.preventDefault();
          }}
        >
          <center>SEARCH BY CITY</center>
          <br />
          <input
            type="text"
            placeholder="Enter a city"
            value={value}
            onChange={(e) => setValue(e.target.value)}
          ></input>
          <input type="submit" value="Search"></input>
        </form></p>
      ) : null}

      {data && data.name && data.name.toLowerCase() === value.toLowerCase() ? (
        <p className="cityResult">
          {firstUpper(value)}
          <br />
          Population: {data.population}
        </p>
      ) : null}

      {value &&
      data &&
      data.name &&
      data.name.toLowerCase() !== value.toLowerCase() ? (
        <p className="notFullCityName">Not a full city name</p>
      ) : null}
      {value && !data ? <p className="notValidInput">Not valid</p> : null}
    </div>
  );
}

export default City;